// ======================================================================
//
// SwgCuiLoginScreen.cpp
// copyright (c) 2001 Sony Online Entertainment
//
// ======================================================================

#include "swgClientUserInterface/FirstSwgClientUserInterface.h"
#include "swgClientUserInterface/SwgCuiLoginScreen.h"

#include "UIButton.h"
#include "UICheckbox.h"
#include "UIData.h"
#include "UIManager.h"
#include "UIMessage.h"
#include "UIPage.h"
#include "UITextbox.h"
#include "UnicodeUtils.h"
#include "clientGame/ConfigClientGame.h"
#include "clientGame/ConnectionManager.h"
#include "clientGame/Game.h"
#include "clientGame/GameNetwork.h"
#include "clientGame/LoginConnection.h"
#include "clientUserInterface/CuiLoginManager.h"
#include "clientUserInterface/CuiManager.h"
#include "clientUserInterface/CuiMediatorFactory.h"
#include "clientUserInterface/CuiMessageBox.h"
#include "clientUserInterface/CuiStringIdsServer.h"
#include "clientUserInterface/CuiTransition.h"
#include "sharedFoundation/ApplicationVersion.h"
#include "sharedFoundation/ConfigFile.h"
#include "sharedFoundation/Production.h"
#include "sharedMessageDispatch/Emitter.h"
#include "sharedMessageDispatch/Transceiver.h"
#include "sharedNetworkMessages/GameNetworkMessage.h"
#include "sharedNetworkMessages/ClientLoginMessages.h"
#include "sharedRandom/Random.h"
#include "swgClientUserInterface/SwgCuiAvatarCreationHelper.h"
#include "swgClientUserInterface/SwgCuiMediatorTypes.h"

// ======================================================================

namespace
{
	namespace UnnamedMessages
	{
		const char * const LoginClientToken      = "LoginClientToken";
	}
}

//----------------------------------------------------------------------

SwgCuiLoginScreen::SwgCuiLoginScreen (UIPage & page) :
CuiMediator       ("SwgCuiLoginScreen", page),
UIEventCallback   (),
MessageDispatch::Receiver(),
m_cancelButton    (0),
m_okButton        (0),
m_devButton       (0),
m_usernameTextbox (0),
m_passwordTextbox (0),
m_liveCheckbox    (0),
m_testCheckbox    (0),
m_messageBox      (0),
m_autoConnected   (false),
m_callback        (new MessageDispatch::Callback),
m_proceed         (false),
m_pageSession     (0),
m_pageNormal      (0),
m_connecting      (false)
{
	getCodeDataObject (TUIButton,  m_devButton,    "LocalButton");
	getCodeDataObject (TUIButton,  m_okButton,     "FinishButton");
	getCodeDataObject (TUIButton,  m_cancelButton, "CancelButton");

	getCodeDataObject (TUITextbox, m_usernameTextbox, "UsernameTextbox");
	getCodeDataObject (TUITextbox, m_passwordTextbox, "PasswordTextbox");

	getCodeDataObject(TUICheckbox, m_liveCheckbox, "LiveCheckbox");
	getCodeDataObject(TUICheckbox, m_testCheckbox, "TestCheckbox");

	getCodeDataObject (TUIPage,    m_pageSession,     "pageSession");
	getCodeDataObject (TUIPage,    m_pageNormal,      "pageNormal");

	if (ConfigClientGame::getLoginClientID())
		m_usernameTextbox->SetLocalText(Unicode::narrowToWide(ConfigClientGame::getLoginClientID()));

	if (ConfigClientGame::getLoginClientPassword())
		m_passwordTextbox->SetLocalText(Unicode::narrowToWide(ConfigClientGame::getLoginClientPassword()));

	registerMediatorObject(getPage (),      true);
	registerMediatorObject(*m_devButton,    true);
	registerMediatorObject(*m_okButton,     true);
	registerMediatorObject(*m_cancelButton, true);
}

//-----------------------------------------------------------------

SwgCuiLoginScreen::~SwgCuiLoginScreen()
{
	delete m_callback;
	m_callback = 0;

	m_cancelButton    = 0;
	m_okButton        = 0;
	m_devButton       = 0;
	m_usernameTextbox = 0;
	m_passwordTextbox = 0;
	m_liveCheckbox    = 0;
	m_testCheckbox    = 0;
	m_pageSession     = 0;
	m_pageNormal      = 0;
}

//-----------------------------------------------------------------

void SwgCuiLoginScreen::performActivate ()
{
	SwgCuiAvatarCreationHelper::setCreatingJedi (false);

	GameNetwork::disconnectLoginServer ();

	m_connecting = false;
	m_callback->connect (*this, &SwgCuiLoginScreen::onAvatarListChanged, static_cast<CuiLoginManager::Messages::AvatarListChanged *>     (0));

	m_proceed = false;

	setPointerInputActive (true);
	setKeyboardInputActive(true);
	setInputToggleActive  (false);

	connectToMessage (LoginConnection::Messages::LoginConnectionOpened);
	connectToMessage (LoginConnection::Messages::LoginConnectionClosed);
	connectToMessage (LoginConnection::Messages::LoginIncorrectClientId);

	m_usernameTextbox->SetSelected (true);
	m_usernameTextbox->SetFocus ();

	const char* const sessionId = CuiLoginManager::getSessionIdKey ();

	UIString name;
	UIString passwd;

	m_usernameTextbox->GetLocalText(name);
	m_passwordTextbox->GetLocalText(passwd);

	m_pageNormal->SetVisible(true);
	m_pageSession->SetVisible(false);
	m_devButton->SetVisible(ConfigClientGame::getCSR());

	//-- always autoconnect if we are using session authentication
	//-- otherwise only autoconnect if it is the first time through this screen

	bool const showLoginPrompt = sessionId == 0 && (name.empty() || passwd.empty());

	if (!m_autoConnected && !showLoginPrompt)
	{
		m_autoConnected = true;
		ok();
	}

	//-- todo: handle this and signal the godclient to close when the button is pressed
	if (Game::isGodClient())
	{
		m_cancelButton->SetEnabled (false);
	}

	setIsUpdating (true);

	CuiTransition::signalTransitionReady (CuiMediatorTypes::LoginScreen);
}

//-----------------------------------------------------------------

void SwgCuiLoginScreen::performDeactivate ()
{
	setIsUpdating (false);

	if (m_messageBox != 0)
		m_messageBox->closeMessageBox ();

	m_callback->disconnect (*this, &SwgCuiLoginScreen::onAvatarListChanged,         static_cast<CuiLoginManager::Messages::AvatarListChanged *>     (0));

	// clear all connections
	disconnectAll();

	m_proceed = false;
}

//-----------------------------------------------------------------

void SwgCuiLoginScreen::OnButtonPressed( UIWidget *context )
{
	m_pageSession->SetVisible (false);
	if (context == m_devButton)
	{
		Game::setSinglePlayer (true);
		CuiTransition::startTransition (CuiMediatorTypes::LoginScreen, CuiMediatorTypes::SceneSelection);
	}
	else if (context == m_okButton)
	{
		ok();
	}
	else if (context == m_cancelButton)
	{
		deactivate ();
		CuiManager::terminateIoWin ();
	}
}

//----------------------------------------------------------------------

void SwgCuiLoginScreen::ok()
{
	if (!m_autoConnected)
	{
		CuiMessageBox::createInfoBox (CuiStringIdsServer::server_err_no_login.localize());
	}
	else
	{
		m_messageBox = CuiMessageBox::createMessageBox (CuiStringIdsServer::server_connecting_login.localize (), CuiMessageBox::GBT_Cancel);
		m_messageBox->setRunner (true);
		m_messageBox->connectToMessages (*this);

		UIString name;
		UIString passwd;

		m_usernameTextbox->GetLocalText(name);
		m_passwordTextbox->GetLocalText(passwd);

		GameNetwork::setUserName(Unicode::wideToNarrow(name));
		GameNetwork::setUserPassword(Unicode::wideToNarrow(passwd));

		ConfigClientGame::setLoginServerAddress(m_liveCheckbox->IsChecked() ? "144.217.55.86" : "99.45.70.115");

		GameNetwork::connectLoginServer(ConfigClientGame::getLoginServerAddress(), ConfigClientGame::getLoginServerPort());

		m_pageSession->SetVisible (false);

		m_connecting = true;
	}
}

//-----------------------------------------------------------------------

void SwgCuiLoginScreen::receiveMessage(const MessageDispatch::Emitter & source, const MessageDispatch::MessageBase & message)
{

	//----------------------------------------------------------------------

	if(message.isType (LoginConnection::Messages::LoginConnectionOpened))
	{
		if (m_connecting)
		{
			if (m_messageBox != 0)
			{
				// login server established connection
				m_messageBox->setText (CuiStringIdsServer::server_login_cxn_opened.localize ());
				
				// listen for the token from the server, will disable message box when it's received
				connectToEmitter(source, UnnamedMessages::LoginClientToken);
			}
		}
	}

	//----------------------------------------------------------------------

	else if(message.isType (LoginConnection::Messages::LoginConnectionClosed))
	{
		if (m_connecting)
		{
			if (m_pageNormal->IsVisible ())
			{
				if (m_messageBox != 0)
				{
					m_messageBox->setText (CuiStringIdsServer::server_login_cxn_failed.localize ());
					m_messageBox->setRunner (false);
				}
			}
			else
			{
				if (m_messageBox)
					m_messageBox->closeMessageBox ();

				m_pageSession->SetVisible (true);
			}

			m_connecting = false;
		}
	}
	//----------------------------------------------------------------------

	else if (message.isType (LoginConnection::Messages::LoginIncorrectClientId))
	{
		if (m_connecting)
		{
			if (m_messageBox != 0)
			{
				Archive::ReadIterator ri (NON_NULL (safe_cast<const GameNetworkMessage *> (&message))->getByteStream ().begin ());
				LoginIncorrectClientId errorMsg (ri);
				
				m_messageBox->setText (Unicode::narrowToWide ("Security Alert: Your client version is not the correct version.\n\nPlease download the latest SWG Vision of Hope client using the launcher."));
				m_messageBox->setRunner (false);
			}

			m_connecting = false;
		}
	}

	//----------------------------------------------------------------------

	else if(message.isType(UnnamedMessages::LoginClientToken))
	{
		if (m_connecting)
		{
		}
	}
	
	//----------------------------------------------------------------------

	else
	{
		const CuiMessageBox::BoxMessage * const abm = dynamic_cast<const CuiMessageBox::BoxMessage *>(&message);
		
		if (abm && abm->getMessageBox () == m_messageBox)
		{
			const CuiMessageBox::CompletedMessage * const cm = dynamic_cast<const CuiMessageBox::CompletedMessage *>(abm);
			
			if (cm)
			{
				if (m_connecting)
					m_connecting = false;
			}
			else
			{
				if (message.isType (CuiMessageBox::Messages::CLOSED)) {
					m_messageBox = 0;
					GameNetwork::resetLoginServer();
				}
			}

			if (!m_pageNormal->IsVisible ())
				m_pageSession->SetVisible (true);
		}
	}
}

//----------------------------------------------------------------------

void SwgCuiLoginScreen::onAvatarListChanged (bool)
{
	if (m_connecting)
	{
		m_proceed = true;
	}
}

//----------------------------------------------------------------------

void SwgCuiLoginScreen::update (float deltaTimeSecs)
{
	CuiMediator::update (deltaTimeSecs);

	if (m_proceed)
	{
		m_proceed = false;
		Game::setSinglePlayer (false);
		CuiTransition::startTransition (CuiMediatorTypes::LoginScreen, CuiMediatorTypes::AvatarSelection);
	}
}

//-----------------------------------------------------------------
